import random

def recherche():
    """
    Cette fonction effectue une série d'exercices basés sur une liste de personnes.

    Exercice 1 : Mélange la liste de personnes et recherche si quelqu'un aime le camping ou a plus de 50 ans.
    Exercice 2 : Affiche les personnes qui aiment le camping ou ont plus de 50 ans.
    Exercice 3 : Affiche les personnes ayant le même nombre de lettres dans leur nom et prénom.
    Exercice 4 : Regroupe les personnes par prénom et affiche les prénoms identiques.
    Exercice 5 : Regroupe les personnes par nom de famille et affiche les noms identiques.
    Exercice 6 : Regroupe les personnes par âge et affiche les âges identiques.
    """
    print("\nExercice 1")
    # Déclaration de la liste de personnes
    group = [["Pascal","ROSE",43],
             ["Mickaël", "FLEUR", 29],
             ["Henri", "TULIPE", 35],
             ["Michel", "FRAMBOISE", 35],
             ["Arthur", "PETALE", 35],
             ["Michel", "POLLEN", 50],
             ["Michel", "FRAMBOISE", 42]]

    group1 = group
    group2 = group

    # Mélanger la liste de personnes
    random.shuffle(group)

    # Exercice 1 : Vérifie si quelqu'un aime le camping ou a plus de 50 ans.
    likercamping = any(person[0] == "Michel" or person[2] >= 50 for person in group)
    if likercamping:
        print("On a dans la liste quelqu'un qui adore le camping !")

    print("\nExercice 2")

    # Exercice 2 : Affiche les personnes qui aiment le camping ou ont plus de 50 ans.
    for person in group:
        if person[0] == "Michel" or person[2] >=50:
            print(f"C'est Formidable {person[0]} {person[1]} {person[2]} ans adore le camping ")

    print("\nExercice 3")

    # Exercice 3 : Affiche les personnes ayant le même nombre de lettres dans leur nom et prénom.
    for person in group:
        if len(person[0]) == len(person[1]):
            print(f"C'est formidable {person[0]} {person[1]} {person[2]} à ans {len(person[0])} le meme nombre de lettre  dans son Nom et Prénom ")

    print("\nExercice 4")

    # Exercice 4 : Regroupe les personnes par prénom et affiche les prénoms identiques.
    prenom_dictio = {}
    for person in group:
        prenom = person[0]
        if prenom in prenom_dictio:
            prenom_dictio[prenom].append(person)
        else:
            prenom_dictio[prenom] = [person]

    for prenom, group in prenom_dictio.items():
        if len(group) > 1:
            print(" et ".join([f"{meme_prenom[0]} {meme_prenom[1]} qui a {meme_prenom[2]} ans" for meme_prenom in group]), "ont des prénoms identiques")

    print("\nExercice 5")

    # Exercice 5 : Regroupe les personnes par nom de famille et affiche les noms identiques.
    dictio = {}
    for per in group1:
        pre = per[1]
        if pre in dictio:
            dictio[pre].append(per)
        else:
            dictio[pre] = [per]

    for pre, group1 in dictio.items():
        if len(group1) > 1:
            print(" et ".join([f"{meme_non[0]} {meme_non[1]} qui a {meme_non[2]} ans" for meme_non in group1]), "ont des noms identiques")

    print("\nExercice 6")

    # Exercice 6 : Regroupe les personnes par âge et affiche les âges identiques.
    nom_dictio = {}
    for person1 in group2:
        nom = person1[2]
        if nom in nom_dictio:
            nom_dictio[nom].append(person1)
        else:
            nom_dictio[nom] = [person1]

    for nom, group in nom_dictio.items():
        if len(group) > 1:
            print(" et ".join([f"{meme_non[0]} {meme_non[1]} qui a {meme_non[2]} ans" for meme_non in group]), "ont des ages identiques")

# Appelle la fonction recherche pour exécuter les exercices
recherche()